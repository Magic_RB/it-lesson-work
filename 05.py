import tkinter as tk;

root = tk.Tk();
canvas = tk.Canvas(root, width = 480, height = 640);
canvas.pack();

for i in range (0, 5) {
    print(i)
    canvas.create_line(i * 20, i * 20, i * 20 + 20, i * 20);
    canvas.create_line(i * 20 + 20, i * 20, i * 20 + 20, i * 20 + 20);
}

n = 6;
for i in range (0, n) {
    canvas.create_text(150, 100, angle = 360 / 6 * i, text = "                 Python")
}

r = 3;
offset_x = 50
offset_y = 200
for i in range (0, r) {
    offset_x += 80;
    offset_y += 0;

    canvas.create_rectangle(0 + offset_x, 0 + offset_y, 40 + offset_x, 80 + offset_y, fill = "blue");
    canvas.create_rectangle(13.333333 * 2 + offset_x, 80 + offset_y, 40 + offset_x, 120 + offset_y, fill = "blue");
    canvas.create_rectangle(0 + offset_x, 80 + offset_y, 13.333333 + offset_x, 120 + offset_y, fill = "blue");7
    canvas.create_rectangle(5 + offset_x, -30 + offset_y, 35 + offset_x, 0 + offset_y, fill = "blue");

    canvas.create_rectangle(7 + offset_x, -28 + offset_y, 18 + offset_x, -22 + offset_y, fill = "red");
    canvas.create_rectangle(7 + 15 + offset_x, -28 + offset_y, 18 + 15 + offset_x, -22 + offset_y, fill = "red");

    canvas.create_rectangle(-15 + offset_x, 0 + offset_y, 0 + offset_x, 60 + offset_y, fill = "black");
    canvas.create_rectangle(40 + offset_x, 0 + offset_y, 40 + 15 + offset_x, 60 + offset_y, fill = "black");
}

canvas.mainloop();1