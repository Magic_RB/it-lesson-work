import tkinter as tk
import random

canvas = tk.Canvas(width=480, height=640)
canvas.pack()

tuple = ("40", "30", "80", "60")

for i in range(100):
    x = random.randint(0, 480)
    y = random.randint(0, 640)
    i = random.randint(0, 3)

    canvas.create_oval(x, y, x + 50, y + 50, fill = "red")
    canvas.create_oval(x + 5, y + 5, x + 45, y + 45, fill = "white")
    canvas.create_text(x + 25, y + 25, text = tuple[i], font = "Arial 30")

canvas.mainloop()