import random
import tkinter as tk

canvas = tk.Canvas(width = 480, height = 640)
canvas.pack()

for i in range(1000):
    x = random.randint(0, 480)
    y = random.randint(0, 640)

    canvas.create_oval(x, y, 50 + x, y + 50)

canvas.mainloop()