import tkinter as tk
import random as rand

RESOLUTION_X = 1366
RESOLUTION_Y = 768
MARGARET_PETTAL_COUNT = 30
MARGARET_COUNT = 7
VIOLET_COUNT = 7
VIOLET_ANGLES = (0, 20, 170, 190, 340)
MARGARET_CENTER_RADIUS = 20
VIOLET_CENTER_RADIUS = 15

canvas = tk.Canvas(width = RESOLUTION_X, height = RESOLUTION_Y)

canvas.create_rectangle(0, RESOLUTION_Y / 2, RESOLUTION_X, RESOLUTION_Y, fill ="green", outline ="green")
canvas.create_rectangle(0, 0, RESOLUTION_X, RESOLUTION_Y / 2, fill ="blue", outline ="blue")

for i in range (0, 30):
    margin = 20

    x = rand.randint(margin, RESOLUTION_X - margin)
    y = rand.randint(RESOLUTION_Y / 2 + margin, RESOLUTION_Y - margin)

    canvas.create_line(0 + x, 0 + y, 0 + x, y - 50, fill = "darkgreen")
    canvas.create_line(0 + x, 0 + y, -20 + x, y - 50, fill = "darkgreen")
    canvas.create_line(0 + x, 0 + y, 20 + x, y - 50, fill = "darkgreen")

# Margarety, ci take daco
for i in range (0, MARGARET_COUNT):
    margin = 50

    x = rand.randint(margin, RESOLUTION_X - margin)
    y = rand.randint(RESOLUTION_Y / 2 + margin, RESOLUTION_Y - margin)

    canvas.create_oval(-MARGARET_CENTER_RADIUS + x, -MARGARET_CENTER_RADIUS + y, MARGARET_CENTER_RADIUS + x, MARGARET_CENTER_RADIUS + y, fill ="yellow", outline ="yellow")
    for o in range (0, MARGARET_PETTAL_COUNT):
        angle = 360 / MARGARET_PETTAL_COUNT * o

        spaces = ""
        for i in range(0, MARGARET_CENTER_RADIUS, 3):
            spaces += "  "
        canvas.create_text(0 + x, 0 + y, text = spaces + "Margareta", font = "Monospace 12",
                           angle = angle, fill = "white")

# Fialky, ci take daco
for i in range (0, VIOLET_COUNT):
    margin = 50

    x = rand.randint(margin, RESOLUTION_X - margin)
    y = rand.randint(RESOLUTION_Y / 2 + margin, RESOLUTION_Y - margin)

    canvas.create_oval(-VIOLET_CENTER_RADIUS + x, -VIOLET_CENTER_RADIUS + y, VIOLET_CENTER_RADIUS + x,
                       VIOLET_CENTER_RADIUS + y, fill="yellow", outline="yellow")

    spaces = ""
    for i in range (0, VIOLET_CENTER_RADIUS, 3):
        spaces += "  "
    for i in range(0, 5):
        canvas.create_text(0 + x, 0 + y, text=spaces + "Fialka", font="Monospace 12",
                           angle=VIOLET_ANGLES[i] + 90, fill="violet")



canvas.pack()
canvas.mainloop()