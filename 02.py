import tkinter as tk

root = tk.Tk()
canvas = tk.Canvas(root, width=480, height=480)
canvas.pack()

text = tk.Text(canvas)
text.insert(tk.INSERT, "G", "one")
text.insert(tk.INSERT, "o", "two")
text.insert(tk.INSERT, "o", "three")
text.insert(tk.INSERT, "g", "four")
text.insert(tk.INSERT, "l", "five")
text.insert(tk.END, "e", "six")

text.place(width = 6 * 8, height = 18)

text.tag_config("one", foreground = "blue")
text.tag_config("two", foreground = "red")
text.tag_config("three", foreground = "yellow")
text.tag_config("four", foreground = "blue")
text.tag_config("five", foreground = "green")
text.tag_config("six", foreground = "red")

canvas.create_oval(100, 100, 200, 200, fill = "#FF8202", outline = "#FF8202")
canvas.create_text(150, 150, text = "Fanta", fill = "white", font = "Arial 45", angle = "10")
canvas.create_text(150, 150, text = "Fanta", fill = "blue", font = "Arial 40", angle = "10")

canvas.mainloop()